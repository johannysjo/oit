#pragma once

#include <glm/gtc/quaternion.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>

#include <stdint.h>

namespace gfx {

struct Trackball {
    glm::vec2 p = {0.0f, 0.0f};
    glm::quat q = {1.0f, 0.0f, 0.0f, 0.0f};
    bool tracking = false;
    float speed = 0.003f;
    float clamp = 100.0f;
};

void trackball_start_tracking(Trackball &trackball, const glm::vec2 &point);

void trackball_move(Trackball &trackball, const glm::vec2 &point);

void trackball_stop_tracking(Trackball &trackball);

glm::mat4 trackball_get_rotation_matrix(const Trackball &trackball);

} // namespace gfx
