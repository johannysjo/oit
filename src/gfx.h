#pragma once

#include <GL/glew.h>

#include <stdint.h>
#include <unordered_map>

namespace gfx {

struct TextureStorageInfo {
    GLint internal_format;
    GLenum format;
    GLenum type;
    GLint unpack_alignment;
};

struct TextureSamplingInfo {
    GLuint min_filter;
    GLuint mag_filter;
    GLuint wrap;
};

struct Texture2D {
    GLuint texture;
    GLenum target;
    uint32_t samples;
    uint32_t width;
    uint32_t height;
    TextureStorageInfo storage;
    TextureSamplingInfo sampling;
};

void texture_2d_create(Texture2D &texture);

void texture_2d_upload_data(Texture2D &texture, const void *data);

void texture_2d_resize(Texture2D &texture, uint32_t width, uint32_t height);

struct Texture2DArray {
    GLuint texture;
    GLenum target;
    uint32_t width;
    uint32_t height;
    uint32_t depth;
    TextureStorageInfo storage;
    TextureSamplingInfo sampling;
};

void texture_2d_array_create(Texture2DArray &texture);

void texture_2d_array_upload_data(Texture2DArray &texture, uint32_t layer, const void *data);

void texture_2d_array_resize(Texture2DArray &texture, uint32_t width, uint32_t height,
                             uint32_t depth);

struct FBO {
    GLuint fbo;
    uint32_t width;
    uint32_t height;
    std::unordered_map<GLenum, Texture2D> attachments;
    bool resizeable;
};

void fbo_create(FBO &fbo, uint32_t width, uint32_t height, bool resizeable);

void fbo_attach_texture(FBO &fbo, GLenum attachment, Texture2D texture);

bool fbo_is_complete(const FBO &fbo);

void fbo_resize(FBO &fbo, uint32_t width, uint32_t height);

GLuint create_shader_program(const char *vertex_shader_src, const char *fragment_shader_src);

GLuint create_shader_program(const char *compute_shader_src);

} // namespace gfx
