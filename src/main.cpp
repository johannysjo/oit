/// @file
/// @brief Order-independent transparency (OIT) demo.
///
/// This demo implements the following rendering techniques:
/// - Weighted blended OIT (WBOIT)
/// - Weighted average OIT (WAOIT)
/// - Depth peeling
/// - Stochastic OIT (SOIT)
/// - k-buffer (experimental, displays k layers but not necessarily the k closest layers)
/// - Unsorted alpha blending (not OIT, but used for comparison)
///
/// @section LICENSE
///
/// Copyright (C) 2020  Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#include "gfx.h"
#include "gfx_utils.h"
#include "logging.h"
#include "utils.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat3x3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#undef NDEBUG
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <stdint.h>
#include <string>
#include <unordered_map>
#include <vector>

enum TransparencyMode : uint32_t {
    TRANSPARENCY_MODE_WBOIT,
    TRANSPARENCY_MODE_WAOIT,
    TRANSPARENCY_MODE_DEPTH_PEELING,
    TRANSPARENCY_MODE_SOIT,
    TRANSPARENCY_MODE_K_BUFFER,
    TRANSPARENCY_MODE_UNSORTED
};

enum WBOITWeightFunc : uint32_t {
    WBOIT_WEIGHT_FUNC_UNIT,
    WBOIT_WEIGHT_FUNC_DEPTH,
    WBOIT_WEIGHT_FUNC_DEPTH2
};

struct KBuffer {
    gfx::Texture2DArray color_texture;
    gfx::Texture2DArray depth_texture;
    gfx::Texture2D counter_texture;
    uint32_t num_layers;
};

enum TimeStamp : uint32_t {
    TIME_STAMP_TRANSPARENCY,
    TIME_STAMP_TAA,
    TIME_STAMP_TONE_MAPPING,
    TIME_STAMP_BLIT,
    TIME_STAMP_TOTAL,
    NUM_TIME_STAMPS
};

struct Window {
    GLFWwindow *handle = nullptr;
    uint32_t width = 1360;
    uint32_t height = 768;
    uint32_t frame_index = 0;
};

struct Settings {
    uint32_t num_mesh_instances = 25;
    glm::vec4 background_color = {0.4f, 0.4f, 0.4f, 1.0f};
    float opacity = 0.5f;
    float gloss = 0.975f;
    float wrap = 0.1f;
    TransparencyMode transparency_mode = TRANSPARENCY_MODE_WBOIT;
    WBOITWeightFunc wboit_weight_func = WBOIT_WEIGHT_FUNC_DEPTH;
    uint32_t depth_peeling_num_layers = 8;
    uint32_t soit_num_iterations = 1;
    bool soit_animate_noise = false;
    bool taa_enabled = true;
};

struct UISettings {
    bool show_settings_ui = true;
    ImVec2 settings_ui_size = {380, 360};
    ImVec2 settings_ui_pos = {10, 10};
    float settings_ui_alpha = 0.7f;
    bool settings_ui_collapsed = false;

    bool show_profiler_ui = true;
    ImVec2 profiler_ui_size = {300, 150};
    ImVec2 profiler_ui_pos = {400, 10};
    float profiler_ui_alpha = 0.7f;
    bool profiler_ui_collapsed = false;
};

struct Context {
    Window window;
    GLuint timer_queries_tic[NUM_TIME_STAMPS];
    GLuint timer_queries_toc[NUM_TIME_STAMPS];
    float frame_times_ms[NUM_TIME_STAMPS];

    GLuint default_vao = 0;
    std::unordered_map<std::string, GLuint> shaders{};
    std::unordered_map<std::string, gfx::FBO> fbos{};
    KBuffer k_buffer{};
    oit::Mesh mesh{};
    oit::MeshVAO mesh_vao{};
    std::vector<glm::mat4> transforms{};
    std::vector<oit::Material> materials{};
    gfx::Texture2D blue_noise_texture{};
    oit::Camera camera{};
    glm::mat4 view_from_world;
    glm::mat4 proj_from_view;
    gfx::Trackball trackball;
    oit::DirectionalLight key_light{};
    oit::HemisphereLight sky_light{};
    uint32_t rng_seed = 1;

    Settings settings;
    UISettings ui_settings;
};

std::string root_dir()
{
    const char *path = getenv("OIT_ROOT");
    const auto path_str = path != nullptr ? std::string(path) : std::string();
    if (path_str.empty()) {
        LOG_ERROR("OIT_ROOT is not set.\n");
        std::exit(EXIT_FAILURE);
    }

    return path_str;
}

float randf()
{
    return float(std::rand()) / RAND_MAX;
}

glm::vec3 rand_vec3()
{
    // FIXME: this produces different results on different compilers, since the evaluation order of
    // constructor parameters is undefined in C++
    return glm::vec3(randf(), randf(), randf());
}

void create_mesh_instances(Context &ctx)
{
    oit::create_sphere_mesh(0.5f, 32, ctx.mesh);
    ctx.mesh_vao = oit::create_mesh_vao(ctx.mesh);

    std::srand(ctx.rng_seed);
    ctx.transforms.reserve(ctx.settings.num_mesh_instances);
    for (uint32_t i = 0; i < ctx.settings.num_mesh_instances; ++i) {
        const glm::vec3 pos = glm::vec3(3.0f, 1.5f, 3.0f) * (2.0f * rand_vec3() - 1.0f);
        const glm::vec3 scale = {1.4f, 1.4f, 1.4f};
        const glm::mat4 transform = glm::scale(glm::translate(glm::mat4(1.0f), pos), scale);
        ctx.transforms.push_back(transform);
    }

    ctx.materials.reserve(ctx.settings.num_mesh_instances);
    for (uint32_t i = 0; i < ctx.settings.num_mesh_instances; ++i) {
        oit::Material material;
        material.base_color = rand_vec3();
        material.metalness = 0.0f;
        material.gloss = ctx.settings.gloss;
        material.wrap = 0.1f;
        material.opacity = ctx.settings.opacity;
        ctx.materials.push_back(material);
    };
}

// clang-format off
void load_shaders(Context &ctx)
{
    const std::string shader_dir = root_dir() + "/src/shaders/";

    ctx.shaders["forward_shading"] = oit::load_shader_program(
        shader_dir + "forward_shading.vert",
        shader_dir + "forward_shading.frag",
        shader_dir);

    ctx.shaders["wboit_accum"] = oit::load_shader_program(
        shader_dir + "forward_shading.vert",
        shader_dir + "wboit_accum.frag",
        shader_dir);

    ctx.shaders["wboit_composite"] = oit::load_shader_program(
        shader_dir + "wboit_composite.comp");

    ctx.shaders["waoit_accum"] = oit::load_shader_program(
        shader_dir + "forward_shading.vert",
        shader_dir + "waoit_accum.frag",
        shader_dir);

    ctx.shaders["waoit_composite"] = oit::load_shader_program(
        shader_dir + "waoit_composite.comp");

    ctx.shaders["depth_peeling"] = oit::load_shader_program(
        shader_dir + "forward_shading.vert",
        shader_dir + "depth_peeling.frag",
        shader_dir);

    ctx.shaders["soit"] = oit::load_shader_program(
        shader_dir + "forward_shading.vert",
        shader_dir + "soit.frag",
        shader_dir);

    ctx.shaders["soit_resolve_accum"] = oit::load_shader_program(
        shader_dir + "screen_aligned_triangle.vert",
        shader_dir + "soit_resolve_accum.frag");

    ctx.shaders["k_buffer_build"] = oit::load_shader_program(
        shader_dir + "forward_shading.vert",
        shader_dir + "k_buffer_build.frag",
        shader_dir);

    ctx.shaders["k_buffer_composite"] = oit::load_shader_program(
        shader_dir + "k_buffer_composite.comp");

    ctx.shaders["taa"] = oit::load_shader_program(
        shader_dir + "taa.comp");

    ctx.shaders["tone_mapping"] = oit::load_shader_program(
        shader_dir + "tone_mapping.comp");

    ctx.shaders["blit"] = oit::load_shader_program(
        shader_dir + "screen_aligned_triangle.vert",
        shader_dir + "blit.frag");
}
// clang-format on

// TODO: should allocate and deallocate RTs on demand depending on which OIT technique is used,
// instead of allocating all RTs for all OIT techniques... But this will do for now.
void create_fbos(Context &ctx)
{
    { // WBOIT
        gfx::Texture2D accum_texture;
        accum_texture.target = GL_TEXTURE_2D;
        accum_texture.samples = 1;
        accum_texture.width = ctx.window.width;
        accum_texture.height = ctx.window.height;
        accum_texture.storage = {GL_RGBA16F, GL_RGBA, GL_FLOAT, 1};
        accum_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER};
        gfx::texture_2d_create(accum_texture);

        gfx::Texture2D revealage_texture;
        revealage_texture.target = GL_TEXTURE_2D;
        revealage_texture.samples = 1;
        revealage_texture.width = ctx.window.width;
        revealage_texture.height = ctx.window.height;
        revealage_texture.storage = {GL_R8, GL_RED, GL_UNSIGNED_BYTE, 1};
        revealage_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER};
        gfx::texture_2d_create(revealage_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, accum_texture);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT1, revealage_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["wboit"] = fbo;
    }

    { // WAOIT
        gfx::Texture2D accum_texture;
        accum_texture.target = GL_TEXTURE_2D;
        accum_texture.samples = 1;
        accum_texture.width = ctx.window.width;
        accum_texture.height = ctx.window.height;
        accum_texture.storage = {GL_RGBA16F, GL_RGBA, GL_FLOAT, 1};
        accum_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER};
        gfx::texture_2d_create(accum_texture);

        gfx::Texture2D count_texture;
        count_texture.target = GL_TEXTURE_2D;
        count_texture.samples = 1;
        count_texture.width = ctx.window.width;
        count_texture.height = ctx.window.height;
        count_texture.storage = {GL_R16F, GL_RED, GL_FLOAT, 1};
        count_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER};
        gfx::texture_2d_create(count_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, accum_texture);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT1, count_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["waoit"] = fbo;
    }

    { // Depth peeling
        gfx::Texture2D accum_texture;
        accum_texture.target = GL_TEXTURE_2D;
        accum_texture.samples = 1;
        accum_texture.width = ctx.window.width;
        accum_texture.height = ctx.window.height;
        accum_texture.storage = {GL_RGBA16F, GL_RGBA, GL_FLOAT, 1};
        accum_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER};
        gfx::texture_2d_create(accum_texture);

        gfx::Texture2D peel_texture;
        peel_texture.target = GL_TEXTURE_2D;
        peel_texture.samples = 1;
        peel_texture.width = ctx.window.width;
        peel_texture.height = ctx.window.height;
        peel_texture.storage = {GL_RGBA16F, GL_RGBA, GL_FLOAT, 1};
        peel_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER};
        gfx::texture_2d_create(peel_texture);

        gfx::Texture2D depth_prev_texture;
        depth_prev_texture.target = GL_TEXTURE_2D;
        depth_prev_texture.samples = 1;
        depth_prev_texture.width = ctx.window.width;
        depth_prev_texture.height = ctx.window.height;
        depth_prev_texture.storage = {GL_R32F, GL_RED, GL_FLOAT, 1};
        depth_prev_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER};
        gfx::texture_2d_create(depth_prev_texture);

        gfx::Texture2D depth_texture;
        depth_texture.target = GL_TEXTURE_2D;
        depth_texture.samples = 1;
        depth_texture.width = ctx.window.width;
        depth_texture.height = ctx.window.height;
        depth_texture.storage = {GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT, GL_FLOAT, 1};
        depth_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER};
        gfx::texture_2d_create(depth_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, accum_texture);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT1, peel_texture);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT2, depth_prev_texture);
        gfx::fbo_attach_texture(fbo, GL_DEPTH_ATTACHMENT, depth_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["depth_peeling"] = fbo;
    }

    { // SOIT
        gfx::Texture2D color_texture;
        color_texture.target = GL_TEXTURE_2D_MULTISAMPLE;
        color_texture.samples = 4;
        color_texture.width = ctx.window.width;
        color_texture.height = ctx.window.height;
        color_texture.storage = {GL_RGBA16F, GL_RGBA, GL_FLOAT, 1};
        color_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER};
        gfx::texture_2d_create(color_texture);

        gfx::Texture2D depth_texture;
        depth_texture.target = GL_TEXTURE_2D_MULTISAMPLE;
        depth_texture.samples = 4;
        depth_texture.width = ctx.window.width;
        depth_texture.height = ctx.window.height;
        depth_texture.storage = {GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT, GL_FLOAT, 1};
        depth_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER};
        gfx::texture_2d_create(depth_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, color_texture);
        gfx::fbo_attach_texture(fbo, GL_DEPTH_ATTACHMENT, depth_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["soit"] = fbo;
    }

    { // SOIT accumulation
        gfx::Texture2D color_texture;
        color_texture.target = GL_TEXTURE_2D;
        color_texture.samples = 1;
        color_texture.width = ctx.window.width;
        color_texture.height = ctx.window.height;
        color_texture.storage = {GL_RGBA16F, GL_RGBA, GL_FLOAT, 1};
        color_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER};
        gfx::texture_2d_create(color_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, color_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["soit_resolve_accum"] = fbo;
    }

    { // Scene
        gfx::Texture2D color_texture;
        color_texture.target = GL_TEXTURE_2D;
        color_texture.samples = 1;
        color_texture.width = ctx.window.width;
        color_texture.height = ctx.window.height;
        color_texture.storage = {GL_R11F_G11F_B10F, GL_RGB, GL_FLOAT, 1};
        color_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER};
        gfx::texture_2d_create(color_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, color_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["scene"] = fbo;
    }

    { // TAA
        gfx::Texture2D history_texture;
        history_texture.target = GL_TEXTURE_2D;
        history_texture.samples = 1;
        history_texture.width = ctx.window.width;
        history_texture.height = ctx.window.height;
        history_texture.storage = {GL_RGBA16F, GL_RGBA, GL_FLOAT, 1};
        history_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER};
        gfx::texture_2d_create(history_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, history_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["taa"] = fbo;
    }

    { // Tone mapping
        gfx::Texture2D color_texture;
        color_texture.target = GL_TEXTURE_2D;
        color_texture.samples = 1;
        color_texture.width = ctx.window.width;
        color_texture.height = ctx.window.height;
        color_texture.storage = {GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, 1};
        color_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER};
        gfx::texture_2d_create(color_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, color_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["tone_mapping"] = fbo;
    }
}

KBuffer create_k_buffer(const uint32_t width, const uint32_t height, const uint32_t num_layers)
{
    gfx::Texture2DArray color_texture;
    color_texture.target = GL_TEXTURE_2D_ARRAY;
    color_texture.width = width;
    color_texture.height = height;
    color_texture.depth = num_layers;
    color_texture.storage = {GL_RGBA16F, GL_RGBA, GL_FLOAT, 1};
    color_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
    gfx::texture_2d_array_create(color_texture);

    gfx::Texture2DArray depth_texture;
    depth_texture.target = GL_TEXTURE_2D_ARRAY;
    depth_texture.width = width;
    depth_texture.height = height;
    depth_texture.depth = num_layers;
    depth_texture.storage = {GL_R32F, GL_RED, GL_FLOAT, 1};
    depth_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
    gfx::texture_2d_array_create(depth_texture);

    gfx::Texture2D counter_texture;
    counter_texture.target = GL_TEXTURE_2D;
    counter_texture.width = width;
    counter_texture.height = height;
    counter_texture.storage = {GL_R32UI, GL_RED_INTEGER, GL_UNSIGNED_INT, 1};
    counter_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
    gfx::texture_2d_create(counter_texture);

    return {color_texture, depth_texture, counter_texture, num_layers};
}

void resize_k_buffer(const uint32_t width, const uint32_t height, KBuffer &k_buffer)
{
    assert(k_buffer.num_layers > 0);
    gfx::texture_2d_array_resize(k_buffer.color_texture, width, height, k_buffer.num_layers);
    gfx::texture_2d_array_resize(k_buffer.depth_texture, width, height, k_buffer.num_layers);
    gfx::texture_2d_resize(k_buffer.counter_texture, width, height);
}

void init_camera(Context &ctx)
{
    ctx.camera.eye = glm::vec3(0.0f, 0.0f, 6.0f);
    ctx.camera.center = glm::vec3(0.0f, 0.0f, 0.0f);
    ctx.camera.up = glm::vec3(0.0f, 1.0f, 0.0f);
    ctx.camera.fov = glm::radians(45.0f);
    ctx.camera.aspect = float(ctx.window.width) / ctx.window.height;
    ctx.camera.z_near = 0.1f;
    ctx.camera.z_far = 20.0f;
}

void init_light_sources(Context &ctx)
{
    ctx.key_light.direction = glm::vec3(-1.0f, -1.0f, -4.0f);
    ctx.key_light.color = glm::vec3(1.0f, 1.0f, 1.0f);
    ctx.key_light.intensity = 0.5f;

    ctx.sky_light.up = glm::vec3(0.0f, 1.0f, 0.0f);
    ctx.sky_light.ground_color = glm::vec3(0.1f, 0.1f, 0.1f);
    ctx.sky_light.sky_color = glm::vec3(0.8f, 0.8f, 0.8f);
    ctx.sky_light.intensity = 1.0f;
}

void init_timer_queries(Context &ctx)
{
    glGenQueries(NUM_TIME_STAMPS, ctx.timer_queries_tic);
    glGenQueries(NUM_TIME_STAMPS, ctx.timer_queries_toc);
    for (uint32_t i = 0; i < NUM_TIME_STAMPS; ++i) {
        ctx.frame_times_ms[i] = 0.0f;
    }
}

void init(Context &ctx)
{
    glGenVertexArrays(1, &ctx.default_vao);
    create_mesh_instances(ctx);
    load_shaders(ctx);
    create_fbos(ctx);
    ctx.k_buffer = create_k_buffer(ctx.window.width, ctx.window.height, 4);
    oit::load_rgba8_texture((root_dir() + "/resources/64_64/LDR_RGBA_0.png").c_str(),
                            ctx.blue_noise_texture);
    init_camera(ctx);
    init_light_sources(ctx);
    init_timer_queries(ctx);
}

void update_settings_ui(Context &ctx)
{
    ImGui::SetNextWindowSize(ctx.ui_settings.settings_ui_size, ImGuiSetCond_Once);
    ImGui::SetNextWindowPos(ctx.ui_settings.settings_ui_pos, ImGuiSetCond_Once);
    ImGui::SetNextWindowCollapsed(ctx.ui_settings.settings_ui_collapsed, ImGuiSetCond_Once);
    ImGui::SetNextWindowBgAlpha(ctx.ui_settings.settings_ui_alpha);
    ImGui::Begin("Settings", &ctx.ui_settings.show_settings_ui);

    if (ImGui::CollapsingHeader("Material", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Combo("Transparency mode", (int *)&ctx.settings.transparency_mode,
                     "WBOIT\0Weighted average\0Depth peeling\0Stochastic\0k-buffer\0Unsorted alpha "
                     "blending\0\0");
        if (ctx.settings.transparency_mode == TRANSPARENCY_MODE_WBOIT) {
            ImGui::Combo("Weight function", (int *)&ctx.settings.wboit_weight_func,
                         "Unit\0Depth\0Depth2\0\0");
        }
        if (ctx.settings.transparency_mode == TRANSPARENCY_MODE_DEPTH_PEELING) {
            ImGui::SliderInt("Layers", (int *)&ctx.settings.depth_peeling_num_layers, 1, 20);
        }
        if (ctx.settings.transparency_mode == TRANSPARENCY_MODE_SOIT) {
            ImGui::SliderInt("Iterations", (int *)&ctx.settings.soit_num_iterations, 1, 10);
            ImGui::Checkbox("Animate noise", &ctx.settings.soit_animate_noise);
        }
        if (ctx.settings.transparency_mode == TRANSPARENCY_MODE_K_BUFFER) {
            if (ImGui::SliderInt("Layers (k)", (int *)&ctx.k_buffer.num_layers, 1, 16)) {
                resize_k_buffer(ctx.window.width, ctx.window.height, ctx.k_buffer);
            }
        }

        if (ImGui::SliderFloat("Opacity", &ctx.settings.opacity, 0.0f, 1.0f)) {
            for (uint32_t i = 0; i < ctx.materials.size(); ++i) {
                ctx.materials[i].opacity = ctx.settings.opacity;
            }
        }
        if (ImGui::SliderFloat("Gloss", &ctx.settings.gloss, 0.0f, 1.0f)) {
            for (uint32_t i = 0; i < ctx.materials.size(); ++i) {
                ctx.materials[i].gloss = ctx.settings.gloss;
            }
        }
        if (ImGui::SliderFloat("Wrap", &ctx.settings.wrap, 0.0f, 1.0f)) {
            for (uint32_t i = 0; i < ctx.materials.size(); ++i) {
                ctx.materials[i].wrap = ctx.settings.wrap;
            }
        }
    }

    if (ImGui::CollapsingHeader("Lighting")) {
        ImGui::SliderFloat("Key light intensity", &ctx.key_light.intensity, 0.0f, 1.0f);
        ImGui::SliderFloat("Sky light intensity", &ctx.sky_light.intensity, 0.0f, 1.0f);
    }

    if (ImGui::CollapsingHeader("Background")) {
        ImGui::ColorEdit4("Background color", &ctx.settings.background_color[0]);
    }

    if (ImGui::CollapsingHeader("PostFX")) {
        ImGui::Checkbox("TAA", &ctx.settings.taa_enabled);
    }

    ImGui::End();
}

void profiler_bar(const char *label, const float time_in_ms, const ImVec4 &color)
{
    const float cursor_pos_x = ImGui::GetCursorPosX();
    ImGui::ColorButton("", color, 0, ImVec2(std::fmin(10.0f * time_in_ms, 1000.0f), 0.0f));
    ImGui::SameLine();
    ImGui::SetCursorPosX(cursor_pos_x + 2.0f);
    ImGui::Text("%s %.1f", label, time_in_ms);
}

void update_profiler_ui(Context &ctx)
{
    ImGui::SetNextWindowSize(ctx.ui_settings.profiler_ui_size, ImGuiSetCond_Once);
    ImGui::SetNextWindowPos(ctx.ui_settings.profiler_ui_pos, ImGuiSetCond_Once);
    ImGui::SetNextWindowCollapsed(ctx.ui_settings.profiler_ui_collapsed, ImGuiSetCond_Once);
    ImGui::SetNextWindowBgAlpha(ctx.ui_settings.profiler_ui_alpha);
    ImGui::Begin("Frame times (ms)", &ctx.ui_settings.show_profiler_ui);

    const ImVec4 color = {0.7f, 0.5f, 0.1f, 1.0f};
    profiler_bar("Transparency", ctx.frame_times_ms[TIME_STAMP_TRANSPARENCY], color);
    profiler_bar("TAA", ctx.frame_times_ms[TIME_STAMP_TAA], color);
    profiler_bar("Tone mapping", ctx.frame_times_ms[TIME_STAMP_TONE_MAPPING], color);
    profiler_bar("Blit", ctx.frame_times_ms[TIME_STAMP_BLIT], color);
    profiler_bar("Total", ctx.frame_times_ms[TIME_STAMP_TOTAL], color);

    ImGui::End();
}

void update_transforms(Context &ctx)
{
    ctx.view_from_world = glm::lookAt(ctx.camera.eye, ctx.camera.center, ctx.camera.up) *
                          gfx::trackball_get_rotation_matrix(ctx.trackball);

    ctx.proj_from_view =
        glm::perspective(ctx.camera.fov, ctx.camera.aspect, ctx.camera.z_near, ctx.camera.z_far);

    if (ctx.settings.taa_enabled) {
        const glm::mat4 taa_offset_matrix =
            oit::get_taa_offset_matrix(ctx.window.width, ctx.window.height, ctx.window.frame_index);
        ctx.proj_from_view = taa_offset_matrix * ctx.proj_from_view;
    }
}

void update(Context &ctx)
{
    update_settings_ui(ctx);
    update_profiler_ui(ctx);
    update_transforms(ctx);
}

void reset_gl_states()
{
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    glDisable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glDisable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ZERO);
    glDisable(GL_SAMPLE_ALPHA_TO_COVERAGE);
}

void draw_forward_shading(const GLuint program, const oit::MeshVAO &mesh_vao,
                          const glm::mat4 &world_from_model, const oit::Material &material,
                          const glm::mat4 &view_from_world, const glm::mat4 &proj_from_view,
                          const oit::DirectionalLight &key_light,
                          const oit::HemisphereLight &sky_light)
{
    const glm::mat4 view_from_model = view_from_world * world_from_model;
    const glm::mat4 proj_from_model = proj_from_view * view_from_model;
    const glm::mat3 view_normal_from_model = glm::inverseTranspose(glm::mat3(view_from_model));

    glProgramUniformMatrix4fv(program, 0, 1, GL_FALSE, &view_from_model[0][0]);
    glProgramUniformMatrix4fv(program, 1, 1, GL_FALSE, &proj_from_model[0][0]);
    glProgramUniformMatrix3fv(program, 2, 1, GL_FALSE, &view_normal_from_model[0][0]);
    glProgramUniform3fv(program, 3, 1, &material.base_color[0]);
    glProgramUniform1f(program, 4, material.metalness);
    glProgramUniform1f(program, 5, material.gloss);
    glProgramUniform1f(program, 6, material.wrap);
    glProgramUniform1f(program, 7, material.opacity);
    glProgramUniform3fv(program, 8, 1, &key_light.direction[0]);
    glProgramUniform3fv(program, 9, 1, &key_light.color[0]);
    glProgramUniform1f(program, 10, key_light.intensity);
    glProgramUniform3fv(program, 11, 1, &sky_light.up[0]);
    glProgramUniform3fv(program, 12, 1, &sky_light.ground_color[0]);
    glProgramUniform3fv(program, 13, 1, &sky_light.sky_color[0]);
    glProgramUniform1f(program, 14, sky_light.intensity);

    glUseProgram(program);
    glBindVertexArray(mesh_vao.vao);
    glDrawElements(GL_TRIANGLES, mesh_vao.num_indices, GL_UNSIGNED_INT, 0);
}

void draw_wboit_accum(const GLuint program, const oit::MeshVAO &mesh_vao,
                      const glm::mat4 &world_from_model, const oit::Material &material,
                      const glm::mat4 &view_from_world, const glm::mat4 &proj_from_view,
                      const oit::DirectionalLight &key_light, const oit::HemisphereLight &sky_light,
                      const WBOITWeightFunc wboit_weight_func)
{
    const glm::mat4 view_from_model = view_from_world * world_from_model;
    const glm::mat4 proj_from_model = proj_from_view * view_from_model;
    const glm::mat3 view_normal_from_model = glm::inverseTranspose(glm::mat3(view_from_model));

    glProgramUniformMatrix4fv(program, 0, 1, GL_FALSE, &view_from_model[0][0]);
    glProgramUniformMatrix4fv(program, 1, 1, GL_FALSE, &proj_from_model[0][0]);
    glProgramUniformMatrix3fv(program, 2, 1, GL_FALSE, &view_normal_from_model[0][0]);
    glProgramUniform3fv(program, 3, 1, &material.base_color[0]);
    glProgramUniform1f(program, 4, material.metalness);
    glProgramUniform1f(program, 5, material.gloss);
    glProgramUniform1f(program, 6, material.wrap);
    glProgramUniform1f(program, 7, material.opacity);
    glProgramUniform3fv(program, 8, 1, &key_light.direction[0]);
    glProgramUniform3fv(program, 9, 1, &key_light.color[0]);
    glProgramUniform1f(program, 10, key_light.intensity);
    glProgramUniform3fv(program, 11, 1, &sky_light.up[0]);
    glProgramUniform3fv(program, 12, 1, &sky_light.ground_color[0]);
    glProgramUniform3fv(program, 13, 1, &sky_light.sky_color[0]);
    glProgramUniform1f(program, 14, sky_light.intensity);
    glProgramUniform1ui(program, 15, uint32_t(wboit_weight_func));

    glUseProgram(program);
    glBindVertexArray(mesh_vao.vao);
    glDrawElements(GL_TRIANGLES, mesh_vao.num_indices, GL_UNSIGNED_INT, 0);
}

void dispatch_wboit_composite(const GLuint program, const gfx::Texture2D &accum_texture,
                              const gfx::Texture2D &revealage_texture,
                              const gfx::Texture2D &scene_texture)
{
    glBindTextureUnit(0, accum_texture.texture);
    glBindTextureUnit(1, revealage_texture.texture);
    glBindImageTexture(0, scene_texture.texture, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R11F_G11F_B10F);
    glUseProgram(program);
    glDispatchCompute(scene_texture.width / 8 + 1, scene_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void wboit_render_pass(Context &ctx)
{
    // Initialize RTs
    glBindVertexArray(ctx.default_vao);
    GLenum targets[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
    glNamedFramebufferDrawBuffers(ctx.fbos["wboit"].fbo, 2, targets);
    glm::vec4 accum_clear_value = {0.0f, 0.0f, 0.0f, 0.0f};
    glm::vec4 revealage_clear_value = {1.0f, 0.0f, 0.0f, 0.0f};
    glClearNamedFramebufferfv(ctx.fbos["wboit"].fbo, GL_COLOR, 0, &accum_clear_value[0]);
    glClearNamedFramebufferfv(ctx.fbos["wboit"].fbo, GL_COLOR, 1, &revealage_clear_value[0]);

    // Perform WBOIT accumulation pass
    glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["wboit"].fbo);
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glDisable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunci(0, GL_ONE, GL_ONE);
    glBlendFunci(1, GL_ZERO, GL_ONE_MINUS_SRC_ALPHA);
    const GLuint program = ctx.shaders["wboit_accum"];
    for (uint32_t i = 0; i < ctx.settings.num_mesh_instances; ++i) {
        draw_wboit_accum(program, ctx.mesh_vao, ctx.transforms[i], ctx.materials[i],
                         ctx.view_from_world, ctx.proj_from_view, ctx.key_light, ctx.sky_light,
                         ctx.settings.wboit_weight_func);
    }
    reset_gl_states();

    // Composite WBOIT result with scene
    dispatch_wboit_composite(ctx.shaders["wboit_composite"],
                             ctx.fbos["wboit"].attachments[GL_COLOR_ATTACHMENT0],
                             ctx.fbos["wboit"].attachments[GL_COLOR_ATTACHMENT1],
                             ctx.fbos["scene"].attachments[GL_COLOR_ATTACHMENT0]);
}

void draw_waoit_accum(const GLuint program, const oit::MeshVAO &mesh_vao,
                      const glm::mat4 &world_from_model, const oit::Material &material,
                      const glm::mat4 &view_from_world, const glm::mat4 &proj_from_view,
                      const oit::DirectionalLight &key_light, const oit::HemisphereLight &sky_light)
{
    const glm::mat4 view_from_model = view_from_world * world_from_model;
    const glm::mat4 proj_from_model = proj_from_view * view_from_model;
    const glm::mat3 view_normal_from_model = glm::inverseTranspose(glm::mat3(view_from_model));

    glProgramUniformMatrix4fv(program, 0, 1, GL_FALSE, &view_from_model[0][0]);
    glProgramUniformMatrix4fv(program, 1, 1, GL_FALSE, &proj_from_model[0][0]);
    glProgramUniformMatrix3fv(program, 2, 1, GL_FALSE, &view_normal_from_model[0][0]);
    glProgramUniform3fv(program, 3, 1, &material.base_color[0]);
    glProgramUniform1f(program, 4, material.metalness);
    glProgramUniform1f(program, 5, material.gloss);
    glProgramUniform1f(program, 6, material.wrap);
    glProgramUniform1f(program, 7, material.opacity);
    glProgramUniform3fv(program, 8, 1, &key_light.direction[0]);
    glProgramUniform3fv(program, 9, 1, &key_light.color[0]);
    glProgramUniform1f(program, 10, key_light.intensity);
    glProgramUniform3fv(program, 11, 1, &sky_light.up[0]);
    glProgramUniform3fv(program, 12, 1, &sky_light.ground_color[0]);
    glProgramUniform3fv(program, 13, 1, &sky_light.sky_color[0]);
    glProgramUniform1f(program, 14, sky_light.intensity);

    glUseProgram(program);
    glBindVertexArray(mesh_vao.vao);
    glDrawElements(GL_TRIANGLES, mesh_vao.num_indices, GL_UNSIGNED_INT, 0);
}

void dispatch_waoit_composite(const GLuint program, const gfx::Texture2D &accum_texture,
                              const gfx::Texture2D &count_texture,
                              const gfx::Texture2D &scene_texture)
{
    glBindTextureUnit(0, accum_texture.texture);
    glBindTextureUnit(1, count_texture.texture);
    glBindImageTexture(0, scene_texture.texture, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R11F_G11F_B10F);
    glUseProgram(program);
    glDispatchCompute(scene_texture.width / 8 + 1, scene_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void waoit_render_pass(Context &ctx)
{
    // Initialize RTs
    glBindVertexArray(ctx.default_vao);
    GLenum targets[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
    glNamedFramebufferDrawBuffers(ctx.fbos["waoit"].fbo, 2, targets);
    glm::vec4 accum_clear_value = {0.0f, 0.0f, 0.0f, 0.0f};
    glm::vec4 count_clear_value = {0.0f, 0.0f, 0.0f, 0.0f};
    glClearNamedFramebufferfv(ctx.fbos["waoit"].fbo, GL_COLOR, 0, &accum_clear_value[0]);
    glClearNamedFramebufferfv(ctx.fbos["waoit"].fbo, GL_COLOR, 1, &count_clear_value[0]);

    // Perform WAOIT accumulation pass
    glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["waoit"].fbo);
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glDisable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunci(0, GL_ONE, GL_ONE);
    glBlendFunci(1, GL_ONE, GL_ONE);
    const GLuint program = ctx.shaders["waoit_accum"];
    for (uint32_t i = 0; i < ctx.settings.num_mesh_instances; ++i) {
        draw_waoit_accum(program, ctx.mesh_vao, ctx.transforms[i], ctx.materials[i],
                         ctx.view_from_world, ctx.proj_from_view, ctx.key_light, ctx.sky_light);
    }
    reset_gl_states();

    // Composite WAOIT result with scene
    dispatch_waoit_composite(ctx.shaders["waoit_composite"],
                             ctx.fbos["waoit"].attachments[GL_COLOR_ATTACHMENT0],
                             ctx.fbos["waoit"].attachments[GL_COLOR_ATTACHMENT1],
                             ctx.fbos["scene"].attachments[GL_COLOR_ATTACHMENT0]);
}

void draw_blit(const GLuint program, const gfx::Texture2D &texture)
{
    glBindTextureUnit(0, texture.texture);
    glUseProgram(program);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

void draw_depth_peeling_peel(const GLuint program, const oit::MeshVAO &mesh_vao,
                             const glm::mat4 &world_from_model, const oit::Material &material,
                             const glm::mat4 &view_from_world, const glm::mat4 &proj_from_view,
                             const oit::DirectionalLight &key_light,
                             const oit::HemisphereLight &sky_light,
                             const gfx::Texture2D &depth_prev_texture)
{
    const glm::mat4 view_from_model = view_from_world * world_from_model;
    const glm::mat4 proj_from_model = proj_from_view * view_from_model;
    const glm::mat3 view_normal_from_model = glm::inverseTranspose(glm::mat3(view_from_model));

    glProgramUniformMatrix4fv(program, 0, 1, GL_FALSE, &view_from_model[0][0]);
    glProgramUniformMatrix4fv(program, 1, 1, GL_FALSE, &proj_from_model[0][0]);
    glProgramUniformMatrix3fv(program, 2, 1, GL_FALSE, &view_normal_from_model[0][0]);
    glProgramUniform3fv(program, 3, 1, &material.base_color[0]);
    glProgramUniform1f(program, 4, material.metalness);
    glProgramUniform1f(program, 5, material.gloss);
    glProgramUniform1f(program, 6, material.wrap);
    glProgramUniform1f(program, 7, material.opacity);
    glProgramUniform3fv(program, 8, 1, &key_light.direction[0]);
    glProgramUniform3fv(program, 9, 1, &key_light.color[0]);
    glProgramUniform1f(program, 10, key_light.intensity);
    glProgramUniform3fv(program, 11, 1, &sky_light.up[0]);
    glProgramUniform3fv(program, 12, 1, &sky_light.ground_color[0]);
    glProgramUniform3fv(program, 13, 1, &sky_light.sky_color[0]);
    glProgramUniform1f(program, 14, sky_light.intensity);

    glBindTextureUnit(0, depth_prev_texture.texture);

    glUseProgram(program);
    glBindVertexArray(mesh_vao.vao);
    glDrawElements(GL_TRIANGLES, mesh_vao.num_indices, GL_UNSIGNED_INT, 0);
}

void depth_peeling_render_pass(Context &ctx)
{
    const GLuint fbo = ctx.fbos["depth_peeling"].fbo;
    const gfx::Texture2D &accum_texture =
        ctx.fbos["depth_peeling"].attachments[GL_COLOR_ATTACHMENT0];
    const gfx::Texture2D &peel_texture =
        ctx.fbos["depth_peeling"].attachments[GL_COLOR_ATTACHMENT1];
    const gfx::Texture2D &depth_prev_texture =
        ctx.fbos["depth_peeling"].attachments[GL_COLOR_ATTACHMENT2];
    const gfx::Texture2D &depth_texture =
        ctx.fbos["depth_peeling"].attachments[GL_DEPTH_ATTACHMENT];
    const GLuint depth_peeling_program = ctx.shaders["depth_peeling"];
    const GLuint blit_program = ctx.shaders["blit"];

    // Initialize RTs
    glBindVertexArray(ctx.default_vao);
    GLenum targets[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT2};
    glNamedFramebufferDrawBuffers(fbo, 2, targets);
    glm::vec4 accum_clear_value = {0.0f, 0.0f, 0.0f, 1.0f};
    glm::vec4 depth_prev_clear_value = {0.0f, 0.0f, 0.0f, 0.0f};
    glClearNamedFramebufferfv(fbo, GL_COLOR, 0, &accum_clear_value[0]);
    glClearNamedFramebufferfv(fbo, GL_COLOR, 1, &depth_prev_clear_value[0]);

    // Peel and blend transparent objects in front-to-back order
    glm::vec4 peel_clear_value = {0.0f, 0.0f, 0.0f, 0.0f};
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    for (uint32_t layer = 0; layer < ctx.settings.depth_peeling_num_layers; ++layer) {
        reset_gl_states();

        glNamedFramebufferDrawBuffer(fbo, GL_COLOR_ATTACHMENT1);
        glClearNamedFramebufferfv(fbo, GL_COLOR, 0, &peel_clear_value[0]);
        glClearNamedFramebufferfi(fbo, GL_DEPTH_STENCIL, 0, 1.0f, 0);
        glEnable(GL_DEPTH_TEST);
        glDepthMask(GL_TRUE);
        for (uint32_t i = 0; i < ctx.settings.num_mesh_instances; ++i) {
            draw_depth_peeling_peel(depth_peeling_program, ctx.mesh_vao, ctx.transforms[i],
                                    ctx.materials[i], ctx.view_from_world, ctx.proj_from_view,
                                    ctx.key_light, ctx.sky_light, depth_prev_texture);
        }

        glBindVertexArray(ctx.default_vao);
        glNamedFramebufferDrawBuffer(fbo, GL_COLOR_ATTACHMENT2);
        glDisable(GL_DEPTH_TEST);
        glDepthMask(GL_FALSE);
        draw_blit(blit_program, depth_texture);

        glNamedFramebufferDrawBuffer(fbo, GL_COLOR_ATTACHMENT0);
        glEnable(GL_BLEND);
        glBlendFuncSeparate(GL_DST_ALPHA, GL_ONE, GL_ZERO, GL_ONE_MINUS_SRC_ALPHA);
        draw_blit(blit_program, peel_texture);
    }
    reset_gl_states();

    // Blend depth peeling result with scene
    glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["scene"].fbo);
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glBlendFuncSeparate(GL_ONE, GL_SRC_ALPHA, GL_ONE, GL_ONE);
    draw_blit(blit_program, accum_texture);
    reset_gl_states();
}

void draw_soit(const GLuint program, const oit::MeshVAO &mesh_vao,
               const gfx::Texture2D &blue_noise_texture, const glm::mat4 &world_from_model,
               const oit::Material &material, const glm::mat4 &view_from_world,
               const glm::mat4 &proj_from_view, const oit::DirectionalLight &key_light,
               const oit::HemisphereLight &sky_light, const int frame_id)
{
    const glm::mat4 view_from_model = view_from_world * world_from_model;
    const glm::mat4 proj_from_model = proj_from_view * view_from_model;
    const glm::mat3 view_normal_from_model = glm::inverseTranspose(glm::mat3(view_from_model));

    glProgramUniformMatrix4fv(program, 0, 1, GL_FALSE, &view_from_model[0][0]);
    glProgramUniformMatrix4fv(program, 1, 1, GL_FALSE, &proj_from_model[0][0]);
    glProgramUniformMatrix3fv(program, 2, 1, GL_FALSE, &view_normal_from_model[0][0]);
    glProgramUniform3fv(program, 3, 1, &material.base_color[0]);
    glProgramUniform1f(program, 4, material.metalness);
    glProgramUniform1f(program, 5, material.gloss);
    glProgramUniform1f(program, 6, material.wrap);
    glProgramUniform1f(program, 7, material.opacity);
    glProgramUniform3fv(program, 8, 1, &key_light.direction[0]);
    glProgramUniform3fv(program, 9, 1, &key_light.color[0]);
    glProgramUniform1f(program, 10, key_light.intensity);
    glProgramUniform3fv(program, 11, 1, &sky_light.up[0]);
    glProgramUniform3fv(program, 12, 1, &sky_light.ground_color[0]);
    glProgramUniform3fv(program, 13, 1, &sky_light.sky_color[0]);
    glProgramUniform1f(program, 14, sky_light.intensity);
    glProgramUniform1i(program, 15, frame_id);

    glBindTextureUnit(0, blue_noise_texture.texture);

    glUseProgram(program);
    glBindVertexArray(mesh_vao.vao);
    glDrawElements(GL_TRIANGLES, mesh_vao.num_indices, GL_UNSIGNED_INT, 0);
}

void draw_soit_resolve_accum(const GLuint program, const gfx::Texture2D &texture,
                             const uint32_t num_samples, const float weight)
{
    glProgramUniform1i(program, 0, num_samples);
    glProgramUniform1f(program, 1, weight);
    glBindTextureUnit(0, texture.texture);
    glUseProgram(program);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

void soit_render_pass(Context &ctx)
{
    // Initialize RTs
    glBindVertexArray(ctx.default_vao);
    glNamedFramebufferDrawBuffer(ctx.fbos["soit"].fbo, GL_COLOR_ATTACHMENT0);
    glNamedFramebufferDrawBuffer(ctx.fbos["soit_resolve_accum"].fbo, GL_COLOR_ATTACHMENT0);
    glm::vec4 clear_color = {0.0f, 0.0f, 0.0f, 0.0f};
    glClearNamedFramebufferfv(ctx.fbos["soit_resolve_accum"].fbo, GL_COLOR, 0, &clear_color[0]);

    for (uint32_t iter = 0; iter < ctx.settings.soit_num_iterations; ++iter) {
        glClearNamedFramebufferfv(ctx.fbos["soit"].fbo, GL_COLOR, 0, &clear_color[0]);
        glClearNamedFramebufferfi(ctx.fbos["soit"].fbo, GL_DEPTH_STENCIL, 0, 1.0f, 0);

        const uint32_t frame_index =
            ctx.settings.soit_animate_noise
                ? ctx.settings.soit_num_iterations * ctx.window.frame_index + iter
                : iter;

        // Render transparent objects
        glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["soit"].fbo);
        glEnable(GL_DEPTH_TEST);
        glDepthMask(GL_TRUE);
        glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE);
        const GLuint program = ctx.shaders["soit"];
        for (uint32_t i = 0; i < ctx.settings.num_mesh_instances; ++i) {
            draw_soit(program, ctx.mesh_vao, ctx.blue_noise_texture, ctx.transforms[i],
                      ctx.materials[i], ctx.view_from_world, ctx.proj_from_view, ctx.key_light,
                      ctx.sky_light, frame_index);
        }
        reset_gl_states();

        // Resolve and accumulate
        glBindVertexArray(ctx.default_vao);
        glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["soit_resolve_accum"].fbo);
        glDisable(GL_DEPTH_TEST);
        glDepthMask(GL_FALSE);
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);
        draw_soit_resolve_accum(ctx.shaders["soit_resolve_accum"],
                                ctx.fbos["soit"].attachments[GL_COLOR_ATTACHMENT0],
                                ctx.fbos["soit"].attachments[GL_COLOR_ATTACHMENT0].samples,
                                1.0f / ctx.settings.soit_num_iterations);
        reset_gl_states();
    }

    // Blend SOIT result with scene
    glBindVertexArray(ctx.default_vao);
    glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["scene"].fbo);
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    draw_blit(ctx.shaders["blit"],
              ctx.fbos["soit_resolve_accum"].attachments[GL_COLOR_ATTACHMENT0]);
    reset_gl_states();
}

void draw_k_buffer_build(const GLuint program, const oit::MeshVAO &mesh_vao,
                         const glm::mat4 &world_from_model, const oit::Material &material,
                         const glm::mat4 &view_from_world, const glm::mat4 &proj_from_view,
                         const oit::DirectionalLight &key_light,
                         const oit::HemisphereLight &sky_light, const KBuffer &k_buffer)
{
    const glm::mat4 view_from_model = view_from_world * world_from_model;
    const glm::mat4 proj_from_model = proj_from_view * view_from_model;
    const glm::mat3 view_normal_from_model = glm::inverseTranspose(glm::mat3(view_from_model));

    glProgramUniformMatrix4fv(program, 0, 1, GL_FALSE, &view_from_model[0][0]);
    glProgramUniformMatrix4fv(program, 1, 1, GL_FALSE, &proj_from_model[0][0]);
    glProgramUniformMatrix3fv(program, 2, 1, GL_FALSE, &view_normal_from_model[0][0]);
    glProgramUniform3fv(program, 3, 1, &material.base_color[0]);
    glProgramUniform1f(program, 4, material.metalness);
    glProgramUniform1f(program, 5, material.gloss);
    glProgramUniform1f(program, 6, material.wrap);
    glProgramUniform1f(program, 7, material.opacity);
    glProgramUniform3fv(program, 8, 1, &key_light.direction[0]);
    glProgramUniform3fv(program, 9, 1, &key_light.color[0]);
    glProgramUniform1f(program, 10, key_light.intensity);
    glProgramUniform3fv(program, 11, 1, &sky_light.up[0]);
    glProgramUniform3fv(program, 12, 1, &sky_light.ground_color[0]);
    glProgramUniform3fv(program, 13, 1, &sky_light.sky_color[0]);
    glProgramUniform1f(program, 14, sky_light.intensity);

    glBindImageTexture(0, k_buffer.color_texture.texture, 0, GL_TRUE, 0, GL_READ_WRITE, GL_RGBA16F);
    glBindImageTexture(1, k_buffer.depth_texture.texture, 0, GL_TRUE, 0, GL_READ_WRITE, GL_R32F);
    glBindImageTexture(2, k_buffer.counter_texture.texture, 0, GL_FALSE, 0, GL_READ_WRITE,
                       GL_R32UI);

    glUseProgram(program);
    glBindVertexArray(mesh_vao.vao);
    glDrawElements(GL_TRIANGLES, mesh_vao.num_indices, GL_UNSIGNED_INT, 0);
}

void dispatch_k_buffer_composite(const GLuint program, const KBuffer &k_buffer,
                                 const gfx::Texture2D &scene_texture)
{
    glBindImageTexture(0, k_buffer.color_texture.texture, 0, GL_TRUE, 0, GL_READ_ONLY, GL_RGBA16F);
    glBindImageTexture(1, k_buffer.depth_texture.texture, 0, GL_TRUE, 0, GL_READ_ONLY, GL_R32F);
    glBindImageTexture(2, k_buffer.counter_texture.texture, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32UI);
    glBindImageTexture(3, scene_texture.texture, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R11F_G11F_B10F);
    glUseProgram(program);
    glDispatchCompute(scene_texture.width / 8 + 1, scene_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void k_buffer_render_pass(Context &ctx)
{
    glBindVertexArray(ctx.default_vao);

    glm::vec4 clear_color = {0.0f, 0.0f, 0.0f, 0.0f};
    glm::vec4 clear_depth = {1.0f, 0.0f, 0.0f, 0.0f};
    glm::uvec4 clear_counter = {0u, 0u, 0u, 0u};
    glClearTexImage(ctx.k_buffer.color_texture.texture, 0, GL_RGBA, GL_FLOAT, &clear_color[0]);
    glClearTexImage(ctx.k_buffer.depth_texture.texture, 0, GL_RED, GL_FLOAT, &clear_depth[0]);
    glClearTexImage(ctx.k_buffer.counter_texture.texture, 0, GL_RED_INTEGER, GL_UNSIGNED_INT,
                    &clear_counter[0]);

    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
    const GLuint program = ctx.shaders["k_buffer_build"];
    for (uint32_t i = 0; i < ctx.settings.num_mesh_instances; ++i) {
        draw_k_buffer_build(program, ctx.mesh_vao, ctx.transforms[i], ctx.materials[i],
                            ctx.view_from_world, ctx.proj_from_view, ctx.key_light, ctx.sky_light,
                            ctx.k_buffer);
        glMemoryBarrier(GL_ALL_BARRIER_BITS);
    }
    reset_gl_states();

    dispatch_k_buffer_composite(ctx.shaders["k_buffer_composite"], ctx.k_buffer,
                                ctx.fbos["scene"].attachments[GL_COLOR_ATTACHMENT0]);
}

void unsorted_alpha_blending_render_pass(Context &ctx)
{
    glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["scene"].fbo);
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    const GLuint program = ctx.shaders["forward_shading"];
    for (uint32_t i = 0; i < ctx.settings.num_mesh_instances; ++i) {
        draw_forward_shading(program, ctx.mesh_vao, ctx.transforms[i], ctx.materials[i],
                             ctx.view_from_world, ctx.proj_from_view, ctx.key_light, ctx.sky_light);
    }
    reset_gl_states();
}

void render_transparent_geom(Context &ctx)
{
    switch (ctx.settings.transparency_mode) {
    case TRANSPARENCY_MODE_WBOIT:
        wboit_render_pass(ctx);
        break;
    case TRANSPARENCY_MODE_WAOIT:
        waoit_render_pass(ctx);
        break;
    case TRANSPARENCY_MODE_DEPTH_PEELING:
        depth_peeling_render_pass(ctx);
        break;
    case TRANSPARENCY_MODE_SOIT:
        soit_render_pass(ctx);
        break;
    case TRANSPARENCY_MODE_K_BUFFER:
        k_buffer_render_pass(ctx);
        break;
    case TRANSPARENCY_MODE_UNSORTED:
        unsorted_alpha_blending_render_pass(ctx);
        break;
    default:
        assert(false && "unknown transparency mode");
    }
}

void dispatch_taa(const GLuint program, const gfx::Texture2D &current_texture,
                  const gfx::Texture2D &history_texture)
{
    glBindTextureUnit(0, current_texture.texture);
    glBindImageTexture(1, history_texture.texture, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA16F);
    glUseProgram(program);
    glDispatchCompute(history_texture.width / 8 + 1, history_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void dispatch_tone_mapping(const GLuint program, const gfx::Texture2D &scene_texture,
                           const gfx::Texture2D &output_texture)
{
    glBindTextureUnit(0, scene_texture.texture);
    glBindImageTexture(0, output_texture.texture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA8);
    glUseProgram(program);
    glDispatchCompute(output_texture.width / 8 + 1, output_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void update_frame_times(Context &ctx)
{
    for (uint32_t i = 0; i < NUM_TIME_STAMPS; ++i) {
        GLuint64 tic_ns;
        glGetQueryObjectui64v(ctx.timer_queries_tic[i], GL_QUERY_RESULT, &tic_ns);
        GLuint64 toc_ns;
        glGetQueryObjectui64v(ctx.timer_queries_toc[i], GL_QUERY_RESULT, &toc_ns);
        const float elapsed_time_ms = toc_ns > tic_ns ? float(toc_ns - tic_ns) / 1.0e6f : 0.0f;
        ctx.frame_times_ms[i] = 0.2f * elapsed_time_ms + 0.8f * ctx.frame_times_ms[i];
    }
}

void display(Context &ctx)
{
    glBindVertexArray(ctx.default_vao);
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_TOTAL], GL_TIMESTAMP);
    reset_gl_states();

    // Clear scene color and depth
    glm::vec4 clear_color = oit::srgb2lin(ctx.settings.background_color);
    glClearNamedFramebufferfv(ctx.fbos["scene"].fbo, GL_COLOR, 0, &clear_color[0]);
    glClearNamedFramebufferfi(ctx.fbos["scene"].fbo, GL_DEPTH_STENCIL, 0, 1.0f, 0);

    // Render transparent geometry
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_TRANSPARENCY], GL_TIMESTAMP);
    render_transparent_geom(ctx);
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_TRANSPARENCY], GL_TIMESTAMP);

    // Apply TAA
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_TAA], GL_TIMESTAMP);
    if (ctx.settings.taa_enabled) {
        dispatch_taa(ctx.shaders["taa"], ctx.fbos["scene"].attachments[GL_COLOR_ATTACHMENT0],
                     ctx.fbos["taa"].attachments[GL_COLOR_ATTACHMENT0]);

        glNamedFramebufferReadBuffer(ctx.fbos["taa"].fbo, GL_COLOR_ATTACHMENT0);
        glNamedFramebufferDrawBuffer(ctx.fbos["scene"].fbo, GL_COLOR_ATTACHMENT0);
        glBlitNamedFramebuffer(ctx.fbos["taa"].fbo, ctx.fbos["scene"].fbo, 0, 0, ctx.window.width,
                               ctx.window.height, 0, 0, ctx.window.width, ctx.window.height,
                               GL_COLOR_BUFFER_BIT, GL_NEAREST);
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_TAA], GL_TIMESTAMP);

    // Apply tone mapping
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_TONE_MAPPING], GL_TIMESTAMP);
    dispatch_tone_mapping(ctx.shaders["tone_mapping"],
                          ctx.fbos["scene"].attachments[GL_COLOR_ATTACHMENT0],
                          ctx.fbos["tone_mapping"].attachments[GL_COLOR_ATTACHMENT0]);
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_TONE_MAPPING], GL_TIMESTAMP);

    // Blit to screen
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_BLIT], GL_TIMESTAMP);
    glNamedFramebufferReadBuffer(ctx.fbos["tone_mapping"].fbo, GL_COLOR_ATTACHMENT0);
    glBlitNamedFramebuffer(ctx.fbos["tone_mapping"].fbo, 0, 0, 0, ctx.window.width,
                           ctx.window.height, 0, 0, ctx.window.width, ctx.window.height,
                           GL_COLOR_BUFFER_BIT, GL_NEAREST);
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_BLIT], GL_TIMESTAMP);

    // Required for ImGui
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_TOTAL], GL_TIMESTAMP);
    update_frame_times(ctx);
}

void error_callback(int /*error*/, const char *description)
{
    LOG_ERROR("%s\n", description);
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    ImGui_ImplGlfw_KeyCallback(window, key, scancode, action, mods);
    if (ImGui::GetIO().WantCaptureKeyboard) {
        return;
    }

    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
    else if (key == GLFW_KEY_R && action == GLFW_PRESS) {
        load_shaders(*ctx);
    }
}

void mouse_button_callback(GLFWwindow *window, const int button, const int action, const int mods)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    ImGui_ImplGlfw_MouseButtonCallback(window, button, action, mods);
    if (ImGui::GetIO().WantCaptureMouse) {
        return;
    }

    double x, y;
    glfwGetCursorPos(window, &x, &y);
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
        gfx::trackball_start_tracking(ctx->trackball, glm::vec2(x, y));
    }
    else if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
        gfx::trackball_stop_tracking(ctx->trackball);
    }
}

void cursor_pos_callback(GLFWwindow *window, const double x, const double y)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    if (ctx->trackball.tracking) {
        gfx::trackball_move(ctx->trackball, glm::vec2(x, y));
    }
}

void scroll_callback(GLFWwindow *window, const double x_offset, const double y_offset)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    ImGui_ImplGlfw_ScrollCallback(window, x_offset, y_offset);
    if (ImGui::GetIO().WantCaptureMouse) {
        return;
    }

    const float zoom_step = glm::radians(1.0f);
    if (y_offset > 0.0) {
        ctx->camera.fov = std::fmax(glm::radians(1.0f), ctx->camera.fov - zoom_step);
    }
    else if (y_offset < 0.0) {
        ctx->camera.fov = std::fmin(glm::radians(180.0f), ctx->camera.fov + zoom_step);
    }
}

void resize_fbos(Context &ctx, const int width, const int height)
{
    for (auto &fbo : ctx.fbos) {
        gfx::fbo_resize(fbo.second, width, height);
        assert(gfx::fbo_is_complete(fbo.second));
    }
}

void framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    ctx->window.width = width;
    ctx->window.height = height;
    ctx->camera.aspect = float(width) / height;
    resize_fbos(*ctx, width, height);
    resize_k_buffer(width, height, ctx->k_buffer);
    glViewport(0, 0, width, height);
}

int main()
{
    Context ctx;

    // Initialize GLFW
    glfwSetErrorCallback(error_callback);
    if (!glfwInit()) {
        std::exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

    ctx.window.handle =
        glfwCreateWindow(ctx.window.width, ctx.window.height,
                         "Order-independent transparency (OIT) zoo", nullptr, nullptr);
    assert(ctx.window.handle != nullptr);
    glfwMakeContextCurrent(ctx.window.handle);

    glfwSetWindowUserPointer(ctx.window.handle, &ctx);
    glfwSetKeyCallback(ctx.window.handle, key_callback);
    glfwSetMouseButtonCallback(ctx.window.handle, mouse_button_callback);
    glfwSetCursorPosCallback(ctx.window.handle, cursor_pos_callback);
    glfwSetScrollCallback(ctx.window.handle, scroll_callback);
    glfwSetFramebufferSizeCallback(ctx.window.handle, framebuffer_size_callback);

    // Initialize GLEW
    glewExperimental = true;
    GLenum status = glewInit();
    if (status != GLEW_OK) {
        LOG_ERROR("%s\n", glewGetErrorString(status));
        std::exit(EXIT_FAILURE);
    }
    LOG_INFO("OpenGL version: %s\n", glGetString(GL_VERSION));

    // Initialize rendering
    init(ctx);

    // Initialize ImGUI
    ImGui::CreateContext();
    ImGui_ImplGlfw_InitForOpenGL(ctx.window.handle, false);
    ImGui_ImplOpenGL3_Init("#version 450");

    // Start rendering loop
    while (!glfwWindowShouldClose(ctx.window.handle)) {
        glfwPollEvents();
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        update(ctx);
        display(ctx);
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        glfwSwapBuffers(ctx.window.handle);
        ctx.window.frame_index += 1;
    }

    // Shut down everything
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
    glfwDestroyWindow(ctx.window.handle);
    glfwTerminate();

    return EXIT_SUCCESS;
}
