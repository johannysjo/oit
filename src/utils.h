#pragma once

#include <GL/glew.h>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <stdint.h>
#include <string>
#include <vector>

namespace gfx { // forward declarations
struct Texture2D;
} // namespace gfx

namespace oit {

struct Material {
    glm::vec3 base_color;
    float metalness;
    float gloss;
    float wrap;
    float opacity;
};

struct DirectionalLight {
    glm::vec3 direction;
    glm::vec3 color;
    float intensity;
};

struct HemisphereLight {
    glm::vec3 up;
    glm::vec3 ground_color;
    glm::vec3 sky_color;
    float intensity;
};

struct Mesh {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<uint32_t> indices;
};

struct MeshVAO {
    GLuint vao;
    GLuint vertex_buffer;
    GLuint normal_buffer;
    GLuint index_buffer;
    uint32_t num_vertices;
    uint32_t num_indices;
};

struct Camera {
    glm::vec3 eye;
    glm::vec3 center;
    glm::vec3 up;
    float fov;
    float aspect;
    float z_near;
    float z_far;
};

glm::vec4 srgb2lin(const glm::vec4 &color);

void create_sphere_mesh(float radius, int32_t resolution, Mesh &mesh);

void load_obj_mesh(const char *filename, Mesh &mesh);

MeshVAO create_mesh_vao(const Mesh &mesh);

void load_rgba8_texture(const char *filename, gfx::Texture2D &texture);

GLuint load_shader_program(const std::string &vertex_shader_filename,
                           const std::string &fragment_shader_filename);

GLuint load_shader_program(const std::string &compute_shader_filename);

GLuint load_shader_program(const std::string &vertex_shader_filename,
                           const std::string &fragment_shader_filename,
                           const std::string &path_to_includes);

glm::mat4 get_taa_offset_matrix(uint32_t width, uint32_t height, uint32_t frame_index);

} // namespace oit
