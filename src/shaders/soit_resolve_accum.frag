#version 450

layout(binding = 0) uniform sampler2DMS u_texture;

layout(location = 0) uniform int u_num_samples;
layout(location = 1) uniform float u_weight;

in VS_OUT {
    vec2 texcoord;
} fs_in;

layout(location = 0) out vec4 frag_color;

void main()
{
    ivec2 texcoord = ivec2(gl_FragCoord.xy);
    vec4 output_color = vec4(0.0);
    for (int i = 0; i < u_num_samples; ++i) {
        vec4 sample_color = texelFetch(u_texture, texcoord, i);
        sample_color = (sample_color.a > 0.0) ? vec4(sample_color.rgb, 1.0) : vec4(0.0);
        output_color += sample_color;
    }
    output_color.rgb /= max(1.0, output_color.a);
    output_color.a /= float(u_num_samples);
    output_color.rgb *= output_color.a;

    frag_color = u_weight * output_color;
}
