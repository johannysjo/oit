#version 450

layout(binding = 0, rgba16f) uniform restrict image2DArray u_color_image;
layout(binding = 1, r32f) uniform restrict image2DArray u_depth_image;
layout(binding = 2, r32ui) uniform restrict uimage2D u_counter_image;

layout(location = 3) uniform vec3 u_base_color;
layout(location = 4) uniform float u_metalness;
layout(location = 5) uniform float u_gloss;
layout(location = 6) uniform float u_wrap;
layout(location = 7) uniform float u_opacity;

layout(location = 8) uniform vec3 u_key_light_direction;
layout(location = 9) uniform vec3 u_key_light_color;
layout(location = 10) uniform float u_key_light_intensity;

layout(location = 11) uniform vec3 u_sky_light_up;
layout(location = 12) uniform vec3 u_sky_light_ground_color;
layout(location = 13) uniform vec3 u_sky_light_sky_color;
layout(location = 14) uniform float u_sky_light_intensity;

in VS_OUT {
    vec3 view_position;
    vec3 view_normal;
} fs_in;

#include "shading_common.glsl"

void main()
{
    ivec2 imcoord = ivec2(gl_FragCoord.xy);
    float depth = gl_FragCoord.z;

    Material material;
    material.base_color = srgb2lin(u_base_color);
    material.metalness = u_metalness;
    material.gloss = u_gloss;
    material.wrap = u_wrap;
    material.opacity = u_opacity;

    DirectionalLight key_light;
    key_light.direction = u_key_light_direction;
    key_light.color = srgb2lin(u_key_light_color) * u_key_light_intensity;

    HemisphereLight sky_light;
    sky_light.up = u_sky_light_up;
    sky_light.ground_color = srgb2lin(u_sky_light_ground_color);
    sky_light.sky_color = srgb2lin(u_sky_light_sky_color);
    sky_light.intensity = u_sky_light_intensity;

    vec3 N = normalize(fs_in.view_normal);
    N = gl_FrontFacing ? N : -N;
    vec3 V = normalize(-fs_in.view_position);

    vec4 output_diffuse = vec4(0.0);
    vec4 output_specular = vec4(0.0);
    compute_shading(N, V, material, key_light, sky_light, output_diffuse, output_specular);

    vec4 output_color = vec4(0.0);
    output_color.rgb += output_diffuse.rgb * output_diffuse.a;
    output_color.rgb += output_specular.rgb * output_specular.a;
    output_color.a = max(output_diffuse.a, output_specular.a);

    const uint k = imageSize(u_color_image).z;
    uint layer = imageAtomicAdd(u_counter_image, imcoord, 1u);
    if (layer < k) {
        imageStore(u_color_image, ivec3(imcoord, layer), output_color);
        imageStore(u_depth_image, ivec3(imcoord, layer), vec4(depth, 0.0, 0.0, 0.0));
    }
}
