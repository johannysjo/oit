#version 450

layout(location = 3) uniform vec3 u_base_color;
layout(location = 4) uniform float u_metalness;
layout(location = 5) uniform float u_gloss;
layout(location = 6) uniform float u_wrap;
layout(location = 7) uniform float u_opacity;

layout(location = 8) uniform vec3 u_key_light_direction;
layout(location = 9) uniform vec3 u_key_light_color;
layout(location = 10) uniform float u_key_light_intensity;

layout(location = 11) uniform vec3 u_sky_light_up;
layout(location = 12) uniform vec3 u_sky_light_ground_color;
layout(location = 13) uniform vec3 u_sky_light_sky_color;
layout(location = 14) uniform float u_sky_light_intensity;

layout(location = 15) uniform uint u_weight_func;

in VS_OUT {
    vec3 view_position;
    vec3 view_normal;
} fs_in;

layout(location = 0) out vec4 rt_accum;
layout(location = 1) out vec4 rt_revealage;

#define WBOIT_WEIGHT_FUNC_UNIT 0
#define WBOIT_WEIGHT_FUNC_DEPTH 1
#define WBOIT_WEIGHT_FUNC_DEPTH2 2

#include "shading_common.glsl"

float wboit_depth_weight(float alpha, float z)
{
    return alpha * max(0.01, 3000.0 * pow(1.0 - 0.99 * z, 3.0));
}

float wboit_depth_weight2(float alpha, float z)
{
    float z_near = 0.1;
    float z_far = 20.0;
    float z_linear = 2.0 * z_near * z_far / (z_far + z_near - (2.0 * z - 1.0) * (z_far - z_near));
    return alpha * max(0.01, min(1000.0, 0.01 / (10e-2 + pow(abs(z_linear) / 8.0, 4.0))));
}

float wboit_weight(uint weight_func, float alpha, float z)
{
    float w = 0.0;
    if (weight_func == WBOIT_WEIGHT_FUNC_UNIT) {
        w = 1.0;
    }
    else if (weight_func == WBOIT_WEIGHT_FUNC_DEPTH) {
        w = wboit_depth_weight(alpha, z);
    }
    else if (weight_func == WBOIT_WEIGHT_FUNC_DEPTH2) {
        w = wboit_depth_weight2(alpha, z);
    }
    return w;
}

void main()
{
    Material material;
    material.base_color = srgb2lin(u_base_color);
    material.metalness = u_metalness;
    material.gloss = u_gloss;
    material.wrap = u_wrap;
    material.opacity = u_opacity;

    DirectionalLight key_light;
    key_light.direction = u_key_light_direction;
    key_light.color = srgb2lin(u_key_light_color) * u_key_light_intensity;

    HemisphereLight sky_light;
    sky_light.up = u_sky_light_up;
    sky_light.ground_color = srgb2lin(u_sky_light_ground_color);
    sky_light.sky_color = srgb2lin(u_sky_light_sky_color);
    sky_light.intensity = u_sky_light_intensity;

    vec3 N = normalize(fs_in.view_normal);
    N = gl_FrontFacing ? N : -N;
    vec3 V = normalize(-fs_in.view_position);

    vec4 output_diffuse = vec4(0.0);
    vec4 output_specular = vec4(0.0);
    compute_shading(N, V, material, key_light, sky_light, output_diffuse, output_specular);

    vec4 output_color = vec4(0.0);
    output_color.rgb += output_diffuse.rgb * output_diffuse.a;
    output_color.rgb += output_specular.rgb * output_specular.a;
    output_color.a = max(output_diffuse.a, output_specular.a);

    float w = wboit_weight(u_weight_func, output_color.a, gl_FragCoord.z);
    rt_accum = vec4(output_color.rgb, output_color.a) * w;
    rt_revealage = vec4(output_color.a);
}
