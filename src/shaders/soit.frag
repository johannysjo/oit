#version 450

layout(binding = 0) uniform sampler2D u_blue_noise_texture;

layout(location = 3) uniform vec3 u_base_color;
layout(location = 4) uniform float u_metalness;
layout(location = 5) uniform float u_gloss;
layout(location = 6) uniform float u_wrap;
layout(location = 7) uniform float u_opacity;

layout(location = 8) uniform vec3 u_key_light_direction;
layout(location = 9) uniform vec3 u_key_light_color;
layout(location = 10) uniform float u_key_light_intensity;

layout(location = 11) uniform vec3 u_sky_light_up;
layout(location = 12) uniform vec3 u_sky_light_ground_color;
layout(location = 13) uniform vec3 u_sky_light_sky_color;
layout(location = 14) uniform float u_sky_light_intensity;

layout(location = 15) uniform int u_frame_index;

in VS_OUT {
    vec3 view_position;
    vec3 view_normal;
} fs_in;

layout(location = 0) out vec4 frag_color;

#include "shading_common.glsl"

float dither2x2(ivec2 pixel)
{
    vec4 pattern = vec4(0.25, 0.75, 0.75, 0.25);
    return pattern[pixel.x % 2 + (pixel.y % 2) * 2];
}

float hash2d(vec2 seed)
{
    return fract(sin(dot(seed, vec2(12.9898, 78.233))) * 43758.5453);
}

float hash4d(vec4 seed)
{
    return hash2d(vec2(hash2d(vec2(hash2d(seed.xy), seed.z)), seed.w));
}

// Modified version of Inigo Quilez' function for generating per-fragment alpha-to-coverage sample
// masks, https://twitter.com/iquilezles/status/947717661496041472. Here we use a simple 4D hash to
// randomize the sample mask and rely on TAA to suppress the noise.
int a2c(float alpha, ivec2 pixel, vec3 view_position, int frame_id)
{
    const int msaa = 4;

    // Dither the input alpha value with a 2x2 checker pattern to increase the number of alpha
    // levels from 5 to 9
    float dither = dither2x2(pixel + ivec2(int(gl_FrontFacing), frame_id % 2));
    alpha = clamp(max(0.125, alpha) + (dither - 0.5) / float(msaa), 0.0, 1.0);

    // Compute alpha-to-coverage sample mask
    int mask = ~(0xff << int(alpha * float(msaa) + 0.5)) & 0xf;

    // Randomize the mask
    float rnd = hash4d(vec4(view_position, frame_id % 1000));
    int shift = int(msaa * rnd) % msaa;
    mask = (((mask << 4) | mask) >> shift) & 0xf; // barrel shift

    return mask;
}

void main()
{
    Material material;
    material.base_color = srgb2lin(u_base_color);
    material.metalness = u_metalness;
    material.gloss = u_gloss;
    material.wrap = u_wrap;
    material.opacity = u_opacity;

    DirectionalLight key_light;
    key_light.direction = u_key_light_direction;
    key_light.color = srgb2lin(u_key_light_color) * u_key_light_intensity;

    HemisphereLight sky_light;
    sky_light.up = u_sky_light_up;
    sky_light.ground_color = srgb2lin(u_sky_light_ground_color);
    sky_light.sky_color = srgb2lin(u_sky_light_sky_color);
    sky_light.intensity = u_sky_light_intensity;

    vec3 N = normalize(fs_in.view_normal);
    N = gl_FrontFacing ? N : -N;
    vec3 V = normalize(-fs_in.view_position);

    vec4 output_diffuse = vec4(0.0);
    vec4 output_specular = vec4(0.0);
    compute_shading(N, V, material, key_light, sky_light, output_diffuse, output_specular);

    vec4 output_color = vec4(0.0);
    output_color.rgb += output_diffuse.rgb * output_diffuse.a;
    output_color.rgb += output_specular.rgb * output_specular.a;
    output_color.a = max(output_diffuse.a, output_specular.a);
    output_color.rgb /= output_color.a;

    int mask = a2c(output_color.a, ivec2(gl_FragCoord.xy), fs_in.view_position, u_frame_index);
    gl_SampleMask[0] = mask;

    frag_color = vec4(output_color.rgb, 1.0);
}
