#version 450

out VS_OUT {
    vec2 texcoord;
} vs_out;

void main()
{
    vs_out.texcoord = 2.0 * vec2(gl_VertexID % 2, gl_VertexID / 2);
    gl_Position = vec4(2.0 * vs_out.texcoord - 1.0, 0.0, 1.0);
}
