struct Material {
    vec3 base_color;
    float metalness;
    float gloss;
    float wrap;
    float opacity;
};

struct DirectionalLight {
    vec3 direction;
    vec3 color;
};

struct HemisphereLight {
    vec3 up;
    vec3 ground_color;
    vec3 sky_color;
    float intensity;
};

vec3 srgb2lin(vec3 color)
{
    return pow(color, vec3(2.2));
}

vec3 lin2srgb(vec3 color)
{
    return pow(color, vec3(0.454));
}

float gloss_to_specular_power(float gloss)
{
    return pow(2.0, 10.0 * gloss + 1.0);
}

float diffuse_wrap(vec3 N, vec3 L, float wrap)
{
    return max(0.0, (dot(N, L) + wrap) / ((1.0 + wrap) * (1.0 + wrap)));
}

float specular_blinn_phong(vec3 N, vec3 H, float specular_power)
{
    return pow(max(0.0, dot(N, H)), specular_power) * (8.0 + specular_power) / 8.0;
}

vec3 fresnel_schlick_gloss(vec3 R_F0, vec3 E, vec3 N, float gloss)
{
    return R_F0 + (max(vec3(gloss), R_F0) - R_F0) * pow(1.0 - max(0.0, dot(E, N)), 5.0);
}

vec3 hemisphere_diffuse(HemisphereLight light, vec3 N)
{
    return mix(light.ground_color, light.sky_color, 0.5 * dot(N, light.up) + 0.5) * light.intensity;
}

vec3 hemisphere_specular(HemisphereLight light, vec3 R, float gloss)
{
    float g = min(0.98, gloss);
    float alpha = clamp(dot(R, light.up) / (1.0 - g * g), 0.0, 1.0);
    return mix(light.ground_color, light.sky_color, alpha) * light.intensity;
}

void compute_shading(vec3 N, vec3 V, Material material, DirectionalLight key_light,
                     HemisphereLight sky_light, out vec4 output_diffuse, out vec4 output_specular)
{
    vec3 L = normalize(-key_light.direction);
    vec3 H = normalize(V + L);
    vec3 R = normalize(reflect(-V, N));

    float specular_power = gloss_to_specular_power(material.gloss);
    vec3 specular_color = mix(vec3(0.04), material.base_color, material.metalness);
    vec3 diffuse_color = mix(material.base_color, vec3(0.0), material.metalness);
    float F0 = max(specular_color.r, max(specular_color.g, specular_color.b));
    vec3 F = fresnel_schlick_gloss(specular_color, V, N, material.gloss);
    float F_max = max(F.r, max(F.g, F.b));

    output_diffuse.rgb = (1.0 - F0) * diffuse_color * diffuse_wrap(N, L, material.wrap) *
        key_light.color;
    output_diffuse.rgb += (1.0 - F_max) * diffuse_color * hemisphere_diffuse(sky_light, N);
    output_diffuse.a = material.opacity;

    output_specular.rgb = F * hemisphere_specular(sky_light, R, material.gloss);
    output_specular.rgb += specular_color * specular_blinn_phong(N, H, specular_power) *
        key_light.color;
    float max_specular = max(output_specular.r, max(output_specular.g, output_specular.b));
    output_specular.a = max(max(F0, F_max * max_specular), material.opacity); // XXX
}
