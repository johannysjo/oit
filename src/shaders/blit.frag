#version 450

layout(binding = 0) uniform sampler2D u_texture;

in VS_OUT {
    vec2 texcoord;
} fs_in;

layout(location = 0) out vec4 frag_color;

void main()
{
    frag_color = texture(u_texture, fs_in.texcoord);
}
