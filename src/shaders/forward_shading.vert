#version 450

layout(location = 0) uniform mat4 u_view_from_model;
layout(location = 1) uniform mat4 u_proj_from_model;
layout(location = 2) uniform mat3 u_view_normal_from_model;

layout(location = 0) in vec4 a_model_position;
layout(location = 1) in vec3 a_model_normal;

out VS_OUT {
    vec3 view_position;
    vec3 view_normal;
} vs_out;

void main()
{
    vs_out.view_position = (u_view_from_model * a_model_position).xyz;
    vs_out.view_normal = u_view_normal_from_model * a_model_normal;
    gl_Position = u_proj_from_model * a_model_position;
}
