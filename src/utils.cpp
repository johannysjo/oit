#include "utils.h"

#include "gfx.h"

#include <glm/common.hpp>
#include <glm/geometric.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#define STBI_ONLY_PNG
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_INCLUDE_LINE_GLSL
#define STB_INCLUDE_IMPLEMENTATION
#include "stb_include.h"

#undef NDEBUG
#include <cassert>
#include <cmath>
#include <cstdio>

namespace {

std::vector<glm::vec3> compute_vertex_normals(const std::vector<glm::vec3> &vertices,
                                              const std::vector<uint32_t> &indices)
{
    std::vector<glm::vec3> normals(vertices.size(), glm::vec3(0.0f, 0.0f, 0.0f));
    for (uint32_t i = 0; i < indices.size(); i += 3) {
        const auto &v0 = vertices[indices[i]];
        const auto &v1 = vertices[indices[i + 1]];
        const auto &v2 = vertices[indices[i + 2]];
        const auto n = glm::cross(v1 - v0, v2 - v0);
        normals[indices[i]] += n;
        normals[indices[i + 1]] += n;
        normals[indices[i + 2]] += n;
    }

    for (uint32_t i = 0; i < normals.size(); ++i) {
        normals[i] /= std::fmax(1.0e-6f, glm::length(normals[i]));
    }

    return normals;
}

GLuint create_buffer(const std::vector<glm::vec3> &data)
{
    assert(!data.empty());

    GLuint buffer;
    glCreateBuffers(1, &buffer);
    const size_t size_in_bytes = data.size() * sizeof(data[0]);
    glNamedBufferStorage(buffer, size_in_bytes, data.data(), GL_DYNAMIC_STORAGE_BIT);

    return buffer;
}

GLuint create_buffer(const std::vector<uint32_t> &data)
{
    assert(!data.empty());

    GLuint buffer;
    glCreateBuffers(1, &buffer);
    const size_t size_in_bytes = data.size() * sizeof(data[0]);
    glNamedBufferStorage(buffer, size_in_bytes, data.data(), GL_DYNAMIC_STORAGE_BIT);

    return buffer;
}

// The R2 low-discrepancy sequence, generates evenly distributed points in [0,1)^2
// Reference: http://extremelearning.com.au/unreasonable-effectiveness-of-quasirandom-sequences/
glm::vec2 ldseq_r2(const float n)
{
    const float phi = 1.324717957244746f;
    const float x = std::fmod(n / phi, 1.0f);
    const float y = std::fmod(n / (phi * phi), 1.0f);

    return {x, y};
}

} // namespace

namespace oit {

glm::vec4 srgb2lin(const glm::vec4 &color)
{
    return glm::pow(color, glm::vec4(2.2f, 2.2f, 2.2f, 1.0f));
}

void create_sphere_mesh(const float radius, const int32_t resolution, Mesh &mesh)
{
    assert(radius >= 0.0f);
    assert(resolution >= 3);
    assert(resolution <= 4096);

    const uint32_t num_vertices = resolution * resolution;
    mesh.vertices.reserve(num_vertices);
    for (int32_t i = 0; i < resolution; ++i) {
        const float theta = glm::pi<float>() * static_cast<float>(i) / (resolution - 1);
        const float sin_theta = std::sin(theta);
        const float cos_theta = std::cos(theta);
        for (int32_t j = 0; j < resolution; ++j) {
            const float phi = 2.0f * glm::pi<float>() * static_cast<float>(j) / resolution;
            const float x = radius * sin_theta * std::cos(phi);
            const float y = radius * sin_theta * std::sin(phi);
            const float z = radius * cos_theta;
            mesh.vertices.push_back(glm::vec3(x, y, z));
        }
    }

    mesh.normals.reserve(num_vertices);
    for (uint32_t i = 0; i < num_vertices; ++i) {
        mesh.normals.push_back(glm::normalize(mesh.vertices[i]));
    }

    const uint32_t num_indices = 6 * resolution * (resolution - 1);
    mesh.indices.reserve(num_indices);
    for (int32_t i = 0; i < resolution - 1; ++i) {
        for (int32_t j = 0; j < resolution; ++j) {
            const uint32_t index0 = i * resolution + j;
            const uint32_t index1 = i * resolution + (j + 1) % resolution;
            const uint32_t index2 = (i + 1) * resolution + j;
            const uint32_t index3 = (i + 1) * resolution + (j + 1) % resolution;
            mesh.indices.insert(mesh.indices.end(), {index0, index2, index3});
            mesh.indices.insert(mesh.indices.end(), {index0, index3, index1});
        }
    }
}

void load_obj_mesh(const char *filename, Mesh &mesh)
{
    std::FILE *fp = std::fopen(filename, "r");
    if (fp == nullptr) {
        std::fclose(fp);
        return;
    }

    char line[256];
    while (std::fgets(line, 256, fp)) {
        char str[2];
        if (line[0] == 'v' && line[1] == ' ') {
            float x, y, z;
            std::sscanf(line, "%s %f %f %f", str, &x, &y, &z);
            mesh.vertices.push_back(glm::vec3(x, y, z));
        }
        else if (line[0] == 'f' && line[1] == ' ') {
            uint32_t index0, index1, index2;
            std::sscanf(line, "%s %u %u %u", str, &index0, &index1, &index2);
            index0 -= 1;
            index1 -= 1;
            index2 -= 1;
            mesh.indices.insert(mesh.indices.end(), {index0, index1, index2});
        }
        else {
            continue;
        }
    }
    std::fclose(fp);

    mesh.normals = compute_vertex_normals(mesh.vertices, mesh.indices);
}

MeshVAO create_mesh_vao(const Mesh &mesh)
{
    assert(mesh.vertices.size() > 0);
    assert(mesh.normals.size() > 0);
    assert(mesh.indices.size() > 0);

    GLuint vao;
    glCreateVertexArrays(1, &vao);

    GLuint vertex_buffer = create_buffer(mesh.vertices);
    const GLuint vertex_binding_idx = 0;
    glVertexArrayVertexBuffer(vao, vertex_binding_idx, vertex_buffer, 0, sizeof(mesh.vertices[0]));
    glEnableVertexArrayAttrib(vao, vertex_binding_idx);
    glVertexArrayAttribFormat(vao, vertex_binding_idx, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(vao, vertex_binding_idx, vertex_binding_idx);

    GLuint normal_buffer = create_buffer(mesh.normals);
    const GLuint normal_binding_idx = 1;
    glVertexArrayVertexBuffer(vao, normal_binding_idx, normal_buffer, 0, sizeof(mesh.normals[0]));
    glEnableVertexArrayAttrib(vao, normal_binding_idx);
    glVertexArrayAttribFormat(vao, normal_binding_idx, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(vao, normal_binding_idx, normal_binding_idx);

    GLuint index_buffer = create_buffer(mesh.indices);
    glVertexArrayElementBuffer(vao, index_buffer);

    MeshVAO mesh_vao{};
    mesh_vao.vao = vao;
    mesh_vao.vertex_buffer = vertex_buffer;
    mesh_vao.normal_buffer = normal_buffer;
    mesh_vao.index_buffer = index_buffer;
    mesh_vao.num_vertices = uint32_t(mesh.vertices.size());
    mesh_vao.num_indices = uint32_t(mesh.indices.size());

    return mesh_vao;
}

void load_rgba8_texture(const char *filename, gfx::Texture2D &texture)
{
    int32_t width;
    int32_t height;
    int32_t num_channels;
    uint8_t *data = stbi_load(filename, &width, &height, &num_channels, 4);
    assert(data != nullptr);

    texture.target = GL_TEXTURE_2D;
    texture.width = width;
    texture.height = height;
    texture.samples = 1;
    texture.storage = {GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, 4};
    texture.sampling = {GL_NEAREST, GL_NEAREST, GL_MIRRORED_REPEAT};
    gfx::texture_2d_create(texture);
    assert(texture.texture != 0);
    gfx::texture_2d_upload_data(texture, data);

    stbi_image_free(data);
}

GLuint load_shader_program(const std::string &vertex_shader_filename,
                           const std::string &fragment_shader_filename)
{
    std::string vshader_src;
    {
        std::FILE *fp = std::fopen(vertex_shader_filename.c_str(), "rb");
        assert(fp != nullptr);
        std::fseek(fp, 0, SEEK_END);
        vshader_src.resize(std::ftell(fp));
        assert(vshader_src.size() > 0);
        std::rewind(fp);
        const size_t num_chars = std::fread(&vshader_src[0], 1, vshader_src.size(), fp);
        assert(num_chars == vshader_src.size());
        std::fclose(fp);
    }

    std::string fshader_src;
    {
        std::FILE *fp = std::fopen(fragment_shader_filename.c_str(), "rb");
        assert(fp != nullptr);
        std::fseek(fp, 0, SEEK_END);
        fshader_src.resize(std::ftell(fp));
        assert(fshader_src.size() > 0);
        std::rewind(fp);
        const size_t num_chars = std::fread(&fshader_src[0], 1, fshader_src.size(), fp);
        assert(num_chars == fshader_src.size());
        std::fclose(fp);
    }

    const GLuint program = gfx::create_shader_program(vshader_src.c_str(), fshader_src.c_str());

    return program;
}

GLuint load_shader_program(const std::string &compute_shader_filename)
{
    std::FILE *fp = std::fopen(compute_shader_filename.c_str(), "rb");
    assert(fp != nullptr);
    std::fseek(fp, 0, SEEK_END);
    std::string cshader_src;
    cshader_src.resize(std::ftell(fp));
    assert(cshader_src.size() > 0);
    std::rewind(fp);
    const size_t num_chars = std::fread(&cshader_src[0], 1, cshader_src.size(), fp);
    assert(num_chars == cshader_src.size());
    std::fclose(fp);

    const GLuint program = gfx::create_shader_program(cshader_src.c_str());

    return program;
}

GLuint load_shader_program(const std::string &vertex_shader_filename,
                           const std::string &fragment_shader_filename,
                           const std::string &path_to_includes)
{
    char *vshader_src;
    {
        // NOTE: we have to create non-const copies of the input strings because stb_include_file()
        // expects non-const char* as input
        std::string filename = {vertex_shader_filename};
        std::string path_to_includes_copy = {path_to_includes};
        char error[256];
        vshader_src = stb_include_file(&filename[0], nullptr, &path_to_includes_copy[0], error);
        assert(vshader_src != nullptr);
    }

    char *fshader_src;
    {
        std::string filename = {fragment_shader_filename};
        std::string path_to_includes_copy = {path_to_includes};
        char error[256];
        fshader_src = stb_include_file(&filename[0], nullptr, &path_to_includes_copy[0], error);
        assert(fshader_src != nullptr);
    }

    const GLuint program = gfx::create_shader_program(vshader_src, fshader_src);

    std::free(fshader_src);
    std::free(vshader_src);

    return program;
}

glm::mat4 get_taa_offset_matrix(const uint32_t width, const uint32_t height,
                                const uint32_t frame_index)
{
    const glm::vec2 texel_size(1.0f / width, 1.0f / height);
    const glm::vec2 rnd = ldseq_r2(float(frame_index % 1000));
    const glm::vec2 offset = texel_size * (2.0f * rnd - 1.0f);

    return glm::translate(glm::mat4(1.0f), glm::vec3(offset[0], offset[1], 0.0f));
}

} // namespace oit
