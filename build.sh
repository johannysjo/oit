#!/bin/bash

export OIT_ROOT=$(pwd)

# Generate build script
cd $OIT_ROOT && \
if [ ! -d build ]; then
    mkdir build
fi
cd build && \
cmake -DCMAKE_INSTALL_PREFIX=$OIT_ROOT ../ && \

# Build and install the program
make -j4 && \
make install && \

# Run the program
cd ../bin && \
./oit
