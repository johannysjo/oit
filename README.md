# Order-independent transparency (OIT) zoo

This OpenGL 4.5 demo implements the following order-independent transparency (OIT) rendering techniques:

- Weighted blended OIT (WBOIT)
- Weighted average OIT (WAOIT)
- Depth peeling
- Stochastic OIT (SOIT)
- k-buffer (experimental, displays k layers but not necessarily the k closest layers)
- Unsorted alpha blending (not OIT, but used for comparison)

I made a short [presentation](https://bitbucket.org/johannysjo/oit/raw/master/oit.pdf) describing and comparing the different rendering techniques, so please check that out :)

## Example screenshot

![Screenshot](https://bitbucket.org/johannysjo/oit/raw/master/screenshot.png)

## Compiling

Clone the git repository and run (from the root folder)
```bash
$ ./build.sh
```
Requires CMake 2.8 or higher and a C++11 capable compiler (GCC 4.6+).

## License

The source code is provided under the MIT license. See LICENSE.txt for more information.
